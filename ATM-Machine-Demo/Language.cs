﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ATM_Machine_Demo
{
    class English
    {
        
        public static void process()
        {
            
            Atm myAtm = new Atm();

            

            Remark();

            var choice = Console.ReadLine();

            while (choice != "1" && choice != "2")
            {
                Console.WriteLine("Invalid input, chose between 1 and 2");
                choice = Console.ReadLine();
            }

            switch (choice)
            {
                case "1":
                    Console.WriteLine("PROCESSING...\n");
                    Console.WriteLine($"Your account balance is {User.AccountBalance}");
                    Rerun();
                    break;

                case "2":
                    withdrawal();
                    Rerun();
                    break;
            }
        }

        public static void Remark()
        {
            Console.WriteLine("Welcome to C# ATM SERVICES \n");
            Console.WriteLine("1.To check balance\n");
            Console.WriteLine("2.To withdraw money\n");
            Console.WriteLine(" Enter your Transaction choice\n");
        }

        public static void Rerun()
        {
            var check = "1";

            Console.WriteLine("do you want to perform any other transaction? \n Press 1 for yes ,\n Press 2 for no");
            check = Console.ReadLine();

            while (check != "1" && check != "2")
            {
                Console.WriteLine("Invalid input, chose between 1 and 2");
                check = Console.ReadLine();
            }
            if (check == "1")
            {
                Remark();
                process();
            }
            else
            {
                Console.WriteLine("take your card");
            }

        }
        public static void withdrawal()

        {
            Console.WriteLine("Enter Amount To Withdraw\n");

            int withdrawal = Convert.ToInt32(Console.ReadLine());

            Atm myAtm = new Atm();
            myAtm.AmountLoaded -= withdrawal;

            while (withdrawal % 500 != 0 || withdrawal % 1000 != 0)
            {
                Console.WriteLine("Enter denomination in multiples of 500 or 1000");
                withdrawal = Convert.ToInt32(Console.ReadLine());
            }

            if (withdrawal <= User.AccountBalance && (withdrawal % 500 == 0 || withdrawal % 1000 == 0) && withdrawal <= myAtm.AmountLoaded)
            {
                Console.WriteLine("PROCESSING ......\n");
                Console.WriteLine("You can now take your cash \n");
                User.AccountBalance -= withdrawal;
                Console.WriteLine($" THE MONEY REMAINING IN THE USERS ACCOUNT AT THIS POINT IS {User.AccountBalance}\n");

                Console.WriteLine($" THE MONEY REMAINING IN THE ATM AT THIS POINT IS {myAtm.AmountLoaded}\n ");
            }
            else if (myAtm.AmountLoaded <= 0)
            {
                Console.WriteLine("Unable to Dispense Cash\n");
            }
            else if (withdrawal > User.AccountBalance)
            {
                Console.WriteLine("Insufficient Account Balance!!!");

            }


            
        }
        }
    }
