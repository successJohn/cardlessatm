﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ATM_Machine_Demo
{
    class Atm
    {
        private int amountLoaded = 10000;
        
        public  int AmountLoaded
        {
            get { return amountLoaded; }
            set { amountLoaded = value; }
        }

        // public int MyProperty { get; set;  do this;
    }
}
